# coding: utf8
from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
import os
from config import ProductionConfig, DevelopmentConfig

db = SQLAlchemy()
migrate = Migrate()

app = Flask(__name__)

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
if (app.config['DEBUG'] == "False"):
    app.config.from_object(ProductionConfig)
    db.init_app(app)
    migrate.init_app(app, db)
else:
    app.config.from_object(DevelopmentConfig)
    db.init_app(app)
    migrate.init_app(app, db)

def create_app(cfg=None):
    if cfg is None:
        app.config.from_object(ProductionConfig)
    elif cfg == "Development":
        app.config.from_object(DevelopmentConfig)
    else:
        app.config.from_object(ProductionConfig)
    db.init_app(app)
    migrate.init_app(app, db)
    return app;

from models import Result

@app.route('/')
def hello():
    if (app.config['DEBUG'] == False):
        return "Hello World Production 333"
    else:
        return "Hello World Development 555!"

@app.route('/name/<val>')
def add(val):
    result = Result(val, 'id', 'idid')
    db.session.add(result)
    db.session.commit()
    peter = Result.query.filter_by(url=val).first()
    return "Hello " + str(peter.url)
    #return "Hello"

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80, Debug=True)