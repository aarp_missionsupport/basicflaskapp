# Basic Flask Application with Postgres and SQL Alchemy#

### Quick summary ###

The purpose of this repository to outline the capabilities of BitBucket Pipeline to perform automated builds and test case execution on a Flask Application The repository also outlines the setup of multiple configurations like Production, Development and provides the ability to the developer to setup the desired configuration during runtime.

### How do I get set up? ###

* Clone the Repository
* Configuration:
* * **Production**: The production configuration leverages the Postgres Database and has Debug = False setup.
* * **Development**: The development configuration leverages a SQL Lite Database at the relative path ./test_db.sqllite but can be changed in the config.py file to point to Postgres database.

The Configuration has been setup by passing -c as the configuration parameter to the python manage.py command.
In the manage.py, the manager.add_option is used to determine if the environment is development or production. Default is set to be ProductionConfig.
Command: python manage.py -c Development runserver

* Dependencies:
All the project dependencies are tracked and installed using the requirements.txt file. The dependencies can be installed via
```
#!bash
pip install -r requirements.txt
```
* Database Configuration:
* * Postgres Setup: 
Create User with Username Test and password Test and a database with name test_db (more about why we need it to be like that in the pipelines configuration)
* * Migrations

**Development** 
```
#!bash
python manage.py -c Development db migrate
python manage.py -c Development db upgrade
```
**Production**
```
#!bash
python manage.py db migrate
python manage.py db upgrade
```
* How to run tests:
The tests are developed in the app_test.py file and are discovered using the Flask-Test module.
```
#!bash
py.test
```

## Bit Bucket Pipeline Integration ##
Bitbucket Pipelines offers a Continuous integration platform leveraging yml definition files and docker like containers / services to define the build machine setup. [Read More](https://bitbucket.org/product/features/pipelines)

### Some highlights: ###
* yml like syntax
* services enables you to add database instances. [Samples](https://confluence.atlassian.com/bitbucket/test-with-databases-in-bitbucket-pipelines-856697462.html) 
* branches lets you define steps for each branch.

### Overview of the bucket-pipelines.yml file in the project ###
* definitions : This area is to define docker like containers and are leveraging docker-compose syntax to build services.
* branches - master : 
* * Run as Development Configuration 
* * Execute test cases on the SQL Lite Database.
* branches - proddeploy : 
* * service - postgres indicates Dependency on the Postgres Service which is built with a database, user name and password.
* * Run as Production Configuration
* * Execute Test Cases on Production Database

### Who do I talk to? ###

* Vinit Patankar ( Amogh Consultants Inc )
* Other community or team contact