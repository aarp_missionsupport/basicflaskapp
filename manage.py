import os
from flask_script import Shell, Command, Manager, Option, Server
from flask_migrate import Migrate, MigrateCommand
from config import ProductionConfig, DevelopmentConfig
from app import create_app, db

def _make_context():
    return dict(app=create_app, db=db)

#app.config.from_object(ProductionConfig)
#app = create_app(ProductionConfig)

manager = Manager(create_app)
manager.add_option('-c', '--config', dest='cfg', required=False)

manager.add_command("shell", Shell(make_context=_make_context))
manager.add_command('db', MigrateCommand)

if __name__ == '__main__':
    manager.run()