pip install virtualenv
virtualenv /home/ubuntu/basicflaskapp/venv
source /home/ubuntu/basicflaskapp/venv/bin/activate
pip install -r requirements.txt
sudo mv /home/ubuntu/basicflaskapp/basicflaskapp.conf /etc/apache2/sites-available
sudo chown -R www-data:www-data basicflaskapp.wsgi
sudo a2enmod wsgi
sudo a2ensite basicflaskapp
sudo apache2 reload
sudo service apache2 restart
if psql -lqt | cut -d \| -f 1 | grep -qw test_db; then
    # database exists
    # $? is 0
else
    psql -c "CREATE USER test WITH PASSWORD 'test';"
    psql -c "create database test_db owner test encoding 'utf-8';"
fi
cd /home/ubuntu/basicflaskapp
python manage.py -c Development db migrate
python manage.py -c Development db upgrade
