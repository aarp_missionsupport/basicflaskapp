sudo apt-get update

sudo apt-get install -y \
    apache2 \
    apache2-dev \
    libapache2-mod-wsgi \
    python3 \
    python3-dev \
    python3-pip

if [ -d /home/ubuntu/basicflaskapp ]; then
  sudo rm -R /home/ubuntu/basicflaskapp
  mkdir /home/ubuntu/basicflaskapp
fi