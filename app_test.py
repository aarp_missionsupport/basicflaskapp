import unittest
from flask import url_for
import pytest
import urllib2
from app import create_app
from config import DevelopmentConfig

@pytest.fixture
def app():
    app = create_app("Development")
    app.config.from_object(DevelopmentConfig)
    return app

@pytest.mark.usefixtures('live_server')
class TestLiveServer:

  def test_add(self):
    response = urllib2.urlopen(url_for('add', val = 'vvv', _external=True))
    assert 1 == 1

  def test_flask_application_is_up_and_running(self):
    response = urllib2.urlopen(url_for('hello', _external=True))
    assert response.code == 200

if __name__ == '__main__':
    unittest.main()
