activate_this = '/home/ubuntu/basicflaskapp/venv/bin/activate_this.py'
#exec(open(activate_this).read())
with open(activate_this) as file_:
    exec(file_.read(), dict(__file__=activate_this))
import sys
sys.path.insert(0, '/home/ubuntu/basicflaskapp')

from app import create_app

application = create_app("Development")