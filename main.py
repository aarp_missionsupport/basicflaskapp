from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

# @app.route('/')
# def hello():
#     return "Hello World 6666!"

def create_app():
    return app;

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True, port=80)